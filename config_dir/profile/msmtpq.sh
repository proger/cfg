if command -v msmtp >/dev/null && ! command -v msmtpq >/dev/null ; then
    export EMAIL_CONN_TEST=n # Use netcat to test internet connection.
    # shellcheck disable=SC2154
    p=$(command -v msmtp)
    macos_dir="$(dirname "$p")/$(dirname "$(readlink "$p")")"
    macos_dir+="/../share/msmtp/scripts/msmtpq"
    for d in "/usr/share/doc/msmtp/examples/msmtpq" "$macos_dir" \
        "/usr/share/doc/msmtp/msmtpq" "/usr/libexec/msmtp/msmtpq" ; do
        if [ -d "$d" ] ; then
            PATH="$PATH:$d"
            break
        fi
    done
    unset p d macos_dir
fi

return 0
