# Use CRAN R studio version if present
# shellcheck disable=SC2154
if [ "$platform" = Darwin ] ; then
    r_framework_bin=/Library/Frameworks/R.framework/Versions/Current/Resources/bin
    if [ -d $r_framework_bin ] ; then
        PATH=$(concat_paths "$PATH" "$r_framework_bin")
    fi
fi

# Set R_HOME and library directory
if command -v R >/dev/null 2>&1 ; then
    export _R_CHECK_LENGTH_1_CONDITION_=true
    export _R_CHECK_LENGTH_1_LOGIC2_=true
    # TODO BUG replace
#       R --slave --no-restore -e 'dir.create(Sys.getenv()[["R_LIBS_USER"]], recursive=TRUE, showWarnings=FALSE)'
    export R_HOME
    R_HOME=$(R --slave --no-restore RHOME)
fi

# Set R_BROWSER
if [ "$platform" = Darwin ] ; then
    export R_BROWSER=open
else
    export R_BROWSER=xdg-open
fi

