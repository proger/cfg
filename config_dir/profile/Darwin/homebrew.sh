if ! command -v brew >/dev/null ; then

    for hbdir in "$HOME/.linuxbrew" /opt/homebrew /usr/local ; do
        if [ -x "$hbdir/bin/brew" ] ; then
            PATH="$hbdir/bin:$PATH"
            export MANPATH="$hbdir/share/man:$MANPATH"
            export INFOPATH="$hbdir/share/info:$MANPATH"
            export PKG_CONFIG_PATH="$hbdir/lib/pkgconfig:$PKG_CONFIG_PATH"
        fi
    done
fi

true
