if ! command -v tclsh >/dev/null && command -v brew >/dev/null ; then
    homebrew_dir=$(dirname "$(which brew)")
    tclsh_bin=$(which "$homebrew_dir/Cellar/tcl-tk/*/bin/tclsh" | tail -n 1)
    if [ -n "$tclsh_bin" ] ; then
            tcltk_dir=$(dirname "$tclsh_bin")
            PATH="$tcltk_dir:$PATH"
    fi
fi
