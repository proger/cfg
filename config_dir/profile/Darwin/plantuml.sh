# Config PlantUML task for Ant
command -v plantuml >/dev/null && \
    export CLASSPATH
    CLASSPATH=$(concat_paths "$(brew --prefix plantuml)/libexec/plantuml.jar" "$CLASSPATH")
