mono_framework_dir=/Library/Frameworks/Mono.framework
if [ -d "$mono_framework_dir" ] ; then
    export LD_LIBRARY_PATH
    LD_LIBRARY_PATH=$(concat_paths "$mono_framework_dir/Versions/Current/lib" "$LD_LIBRARY_PATH")
fi
