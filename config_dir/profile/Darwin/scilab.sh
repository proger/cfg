if ! command -v scilab >/dev/null && \
    ls '/Applications/scilab*.app' >/dev/null 2>&1 ; then
    scilab_bin=$(which '/Applications/scilab*.app/Contents/MacOS/bin/scilab' | tail -n 3)
    if [ -n "$scilab_bin" ] ; then
            scilab_dir=$(dirname "$scilab_bin")
            PATH=$(concat_paths "$scilab_dir" "$PATH")
    fi
fi
