python_dir=$HOME/Library/Python
if [ -d "$python_dir" ] ; then
    python_bin_dir=$(ls -dl "$python_dir/*/bin" 2>/dev/null)
    if [ -d "$python_bin_dir" ] ; then
        PATH="$python_bin_dir:$PATH"
    fi
fi
