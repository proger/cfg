if ! command -v matlab >/dev/null &&
    ls '/Applications/MATLAB*.app' >/dev/null 2>&1 ; then
    matlab_bin=$(which /Applications/MATLAB*.app/bin/matlab | tail -n 1)
    if [ -n "$matlab_bin" ] ; then
            matlab_dir=$(dirname "$matlab_bin")
            PATH="$matlab_dir:$PATH"
    fi
fi
