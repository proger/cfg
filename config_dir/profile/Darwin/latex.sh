for version in 2012 2011 ; do
    texlive="/usr/local/texlive/$version"
    if [ -d "$texlive" ] ; then
        PATH=$(concat_paths "$texlive/bin/x86_64-darwin" "$PATH")
        export MANPATH="$texlive/texmf/doc/man:$MANPATH"
        export TEXINPUTS="$texlive/texmf-dist//:.:$TEXINPUTS"
        break
    fi
done
