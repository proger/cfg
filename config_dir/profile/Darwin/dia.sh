dia_app_bin=/Applications/Dia.app/Contents/Resources/bin
if [ -d $dia_app_bin ] ; then
    PATH=$(concat_paths "$dia_app_bin" "$PATH")
fi

true
