boost_home=/usr/local/Cellar/boost/1.54.0
if [ -e "$boost_home" ] ; then
    export LIBRARY_PATH
    LIBRARY_PATH=$(concat_paths "$boost_home/lib" "$LIBRARY_PATH")
    export CPATH
    CPATH=$(concat_paths "$boost_home/include" "$CPATH")
#   if [ "$platform" = Darwin ] ; then
#       export DYLD_FALLBACK_LIBRARY_PATH=$(concat_paths "$DYLD_FALLBACK_LIBRARY_PATH" $boost_home/lib)
#   else
#       export LD_LIBRARY_PATH=$(concat_paths $boost_home/lib "$LD_LIBRARY_PATH")
#   fi
fi
