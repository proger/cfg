# MYSQL C++ CONNECTOR
mysql_cpp_conn_home=/usr/local/Cellar/mysql-connector-c++/1.1.0
if [ -e "$mysql_cpp_conn_home" ] ; then
    export LIBRARY_PATH
    LIBRARY_PATH=$(concat_paths "$mysql_cpp_conn_home/lib" "$LIBRARY_PATH")
    export CPATH
    CPATH=$(concat_paths "$mysql_cpp_conn_home/include" "$CPATH")
#   if [ "$platform" = Darwin ] ; then
#       export DYLD_FALLBACK_LIBRARY_PATH=$(concat_paths "$DYLD_FALLBACK_LIBRARY_PATH" $mysql_cpp_conn_home/lib)
#   else
#       export LD_LIBRARY_PATH=$(concat_paths $mysql_cpp_conn_home/lib "$LD_LIBRARY_PATH")
#   fi
fi
