if ! command -v port >/dev/null && test -x /opt/local/bin/port ; then
    export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
    export MANPATH="/opt/local/share/man:$MANPATH"
fi
