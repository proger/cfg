cmake_app=/Applications/CMake.app/Contents/
if [ -d "$cmake_app" ] ; then
    PATH=$(concat_paths "$cmake_app/bin" "$PATH")
    export MANPATH="$cmake_app/man:$MANPATH"
fi
