# Cabal binaries
cabal_folder="$HOME/.cabal"
if [ -d "$cabal_folder" ] && [ -d "$cabal_folder/bin" ] ; then
    PATH="$PATH:$cabal_folder/bin"
fi
