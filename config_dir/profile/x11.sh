# Choose best terminal
for term in kitty xfce4-terminal urxvt rxvt xterm ; do
    if command -v "$term" >/dev/null 2>&1 ; then
       export TERMINAL="$term"
       break
    fi
done

x11_home=/usr/X11
if [ -e $x11_home ] ; then
    export LIBRARY_PATH
    LIBRARY_PATH=$(concat_paths "$x11_home/lib" "$LIBRARY_PATH")
    export CPATH
    CPATH=$(concat_paths "$x11_home/include" "$CPATH")
fi
