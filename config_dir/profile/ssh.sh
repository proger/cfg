# Start ssh-agent
# Only as pierrick (i.e.: exclude CNRGH cluster)
test "$USER" = pierrick && \
test -z "$SSH_AGENT_PID" && ls "$HOME"/.ssh/id_* >/dev/null 2>&1 \
    && command -v ssh-agent >/dev/null \
    && eval "$(ssh-agent)" >/dev/null
