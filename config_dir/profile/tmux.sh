export TMUX_BG_COLOR=colour33
export TMUX_FG_COLOR=colour255

# shellcheck disable=SC2154
debug 2 "HOSTNAME=$hstnm"
case "$hstnm" in
    spirou)     export TMUX_BG_COLOR=colour196 ; export TMUX_FG_COLOR=colour255 ;;
    fantasio)   export TMUX_BG_COLOR=colour226 ; export TMUX_FG_COLOR=colour0   ;;
    champignac) export TMUX_BG_COLOR=colour99  ; export TMUX_FG_COLOR=colour255 ;;
    poildur)    export TMUX_BG_COLOR=colour214 ; export TMUX_FG_COLOR=colour232 ;;
    dupilon)    export TMUX_BG_COLOR=colour22  ; export TMUX_FG_COLOR=colour255 ;;
    cyanure)    export TMUX_BG_COLOR=colour207 ; export TMUX_FG_COLOR=colour232 ;;
    bioinfo)    export TMUX_BG_COLOR=colour70  ; export TMUX_FG_COLOR=colour232 ;;
    faunus)     export TMUX_BG_COLOR=colour129 ; export TMUX_FG_COLOR=colour255 ;;
    inti*)      export TMUX_BG_COLOR=colour220 ; export TMUX_FG_COLOR=colour232 ;;
esac
debug 2 "TMUX_BG_COLOR=$TMUX_BG_COLOR"
debug 2 "TMUX_FG_COLOR=$TMUX_FG_COLOR"
