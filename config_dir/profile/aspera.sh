# shellcheck disable=SC2154
case "$platform" in
    Linux)  aspera_dir=$HOME/.aspera/cli ;;
    Darwin) aspera_dir=$HOME/Applications/Aspera\ CLI ;;
esac
if [ -d "$aspera_dir" ] ; then
    PATH=$(concat_paths "$aspera_dir/bin" "$PATH")
    export MANPATH="$aspera_dir/share/man:$MANPATH"
fi

true
