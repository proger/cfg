# Vendor perl bin dir (for cpanm)
insert_in_path "/usr/bin/vendor_perl"

perl5_dir=$HOME/perl5
if [ -d "$perl5_dir" ] ; then
    export MANPATH="$MANPATH:$perl5_dir/man"
    insert_in_path "$perl5_dir/bin"
    export PERL5LIB
    PERL5LIB=$(insert_in "$perl5_dir/lib/perl5" "$PERL5LIB")
    export PERL_LOCAL_LIB_ROOT
    PERL_LOCAL_LIB_ROOT=$(insert_in "$perl5_dir" "$PERL_LOCAL_LIB_ROOT")
    export PERL_MB_OPT="--install_base \"$perl5_dir\""
    export PERL_MM_OPT="INSTALL_BASE=$perl5_dir"
fi

return 0
