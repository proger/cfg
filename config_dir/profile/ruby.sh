if command -v ruby >/dev/null && command -v gem >/dev/null ; then
    gem_dir=$(ruby -r rubygems -e 'puts Gem.user_dir')
    PATH=$(concat_paths "$gem_dir/bin" "$PATH")
fi
