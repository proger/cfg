cuda_home=/usr/local/cuda

# Set env vars
if [ -e "$cuda_home" ] ; then
    PATH=$(concat_paths "$cuda_home/bin" "$PATH")
    export LIBRARY_PATH
    LIBRARY_PATH=$(concat_paths "$cuda_home/lib" "$LIBRARY_PATH")
    export CPATH
    CPATH=$(concat_paths "$cuda_home/include" "$CPATH")
fi
