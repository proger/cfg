lynx_cfg="$HOME/.lynx/lynx.cfg"
if [ -f "$lynx_cfg" ] ; then
    export LYNX_CFG="$lynx_cfg"
fi

return 0
