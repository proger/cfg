ANT_HOME=$(echo /cygdrive/c/apache*)
if [ -d "$ANT_HOME/bin" ] ; then
    export ANT_HOME
    PATH=$(concat_paths "$ANT_HOME/bin" "$PATH")
fi
