if [ -z "$JAVA_HOME" ] ; then
    for java_home_unix in /cygdrive/c/Java/jdk* /cygdrive/c/Program\ Files*/Java/jdk* ; do
        if [ -d "$java_home_unix" ] ; then
            export JAVA_HOME
            JAVA_HOME=$(cygpath -d "$java_home_unix")
            break
        fi
    done
fi
if [ -n "$JAVA_HOME" ] ; then
    PATH=$(concat_paths "$JAVA_HOME/bin" "$PATH")
fi
