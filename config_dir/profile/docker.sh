if ! command -v docker >/dev/null ; then
    # macos
    bin_dir=/Applications/Docker.app/Contents/Resources/bin
    if [ -d "$bin_dir" -a -e "$bin_dir/docker" ] ; then
        PATH="$bin_dir:$PATH"
    fi
fi
