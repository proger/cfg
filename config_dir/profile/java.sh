# To allow display of AWT/Swing GUI on xmonad or other tiling window managers.
export _JAVA_AWT_WM_NONREPARENTING=1

# Use java_home executable
jh=/usr/libexec/java_home
if [ -x $jh ] && [ -z "$JAVA_HOME" ] ; then
    JAVA_HOME=$($jh 2>/dev/null)
    if [ -n "$JAVA_HOME" ] ; then
        export JAVA_HOME
        export JAVA_CPPFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/darwin"
        export JAVA_LIBS="-L$JAVA_HOME/jre/lib/server -L$JAVA_HOME/jre/lib -ljvm"
        export JAVA_LD_LIBRARY_PATH="$JAVA_HOME/jre/lib/server:$JAVA_HOME/jre/lib"
    fi
fi
