# vi: ft=sh cc=82

if command -v scan-repos >/dev/null 2>&1 ; then
    export REPOSPATH=
    for d in Desktop Documents dev data games games/game_saves private \
        private/dev ; do
        if [ -d "$HOME/$d" ] ; then
            if [ -z "$REPOSPATH" ] ; then
                REPOSPATH="$HOME/$d"
            else
                REPOSPATH="$REPOSPATH:$HOME/$d"
            fi
        fi
    done
    debug 1 "scan-repos REPOSPATH=$REPOSPATH"
else
    warning 1 "scan-repos is not installed."
fi
