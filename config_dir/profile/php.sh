pear_home=$HOME/pear

if [ -e "$pear_home" ] ; then

    # PHP.INI
    phpdir="$HOME/.php"
    if [ ! -e "$phpdir" ] ; then
        mkdir "$phpdir"
        cat >>"$phpdir/php.ini" <<EOF
include_path=".:$pear_home/share/pear"
EOF
    fi

    # ENV VARS
    PATH=$(concat_paths "$pear_home/bin" "$PATH")
    export PHPRC="$phpdir"
fi
