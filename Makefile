PRIVATE_DIR=$(firstword $(wildcard $(HOME)/private/private-cfg $(CURDIR)/../private-cfg\
    $(CURDIR)/private))
PRIVATE_CONFIG_DIR=$(addprefix $(HOME)/.config/,$(notdir $(wildcard $(PRIVATE_DIR)/config_dir/*)))
PRIVATE_ROOT_CFG=$(addprefix $(HOME)/.,$(notdir $(wildcard $(PRIVATE_DIR)/root_cfg/*)))

ROOT_CFG_BASE_FILES=Renviron Rprofile bashrc bash_profile dialogrc gitconfig gitignore links lynx lynxrc profile screenrc shellrc tmux.conf vimrc zshrc zprofile vim tmux-plugins alacritty.yml abcde.conf
CONFIG_DIR_BASE_FILES=shell_common profile ranger
CFG_SHELL_FILES=$(shell find config_dir/shell -type f -name '*.sh')

ROOT_CFG=$(addprefix $(HOME)/.,$(notdir $(wildcard root_cfg/*) vim tmux-plugins lscolorsrc))
ifneq ($(DEBUG),)
$(info PRIVATE_DIR=$(PRIVATE_DIR))
$(info PRIVATE_CONFIG_DIR=$(PRIVATE_CONFIG_DIR))
$(info PRIVATE_ROOT_CFG=$(PRIVATE_ROOT_CFG))
$(info ROOT_CFG_BASE_FILES=$(ROOT_CFG_BASE_FILES))
$(info ROOT_CFG=$(ROOT_CFG))
endif

# Get installation mode
MODE=
bioinfo.cng.fr_MODE=cluster
inti.cnrgh.fr_MODE=cluster
cyanure_MODE=perso
spirou_MODE=perso
spip_MODE=perso
champignac_MODE=perso
assurancetourix_MODE=perso
HOSTNAME=$(patsubst %.local,%,$(shell hostname))
ifneq ($(DEBUG),)
$(info HOSTNAME=$(HOSTNAME))
endif
ifdef $(HOSTNAME)_MODE
MODE=$($(HOSTNAME)_MODE)
else
	MODE=perso
endif
ifneq ($(DEBUG),)
$(info MODE=$(MODE))
endif

CONFIG_DIR=$(addprefix $(HOME)/.config/,$(notdir $(wildcard config_dir/*)))

# Restrict files for work computers
ifeq ($(MODE),cluster)
ROOT_CFG:=$(filter $(addprefix $(HOME)/.,$(ROOT_CFG_BASE_FILES)),$(ROOT_CFG))
CONFIG_DIR:=$(filter $(addprefix $(HOME)/.config/,$(CONFIG_DIR_BASE_FILES)),$(CONFIG_DIR))
endif
$(info ROOT_CFG=$(ROOT_CFG))
$(info CONFIG_DIR=$(CONFIG_DIR))

LN=bash $(CURDIR)/createlink.sh

EMPTY_DIRS=$(HOME)/.msmtp.queue $(HOME)/log
# ~/log folder is for msmtp-queue.

ifneq ($(DEBUG),)
$(info CONFIG_DIR=$(CONFIG_DIR))
$(info ROOT_CFG=$(ROOT_CFG))
endif

# Function: Remove special characters
remove_special_chars = $(shell echo "$(1)" | sed 's![^A-Za-z0-9]!_!g')

# Function: Clone Git repos
define clone_git_repos
$(1):
	mkdir -p $$(dir $(1))
	git clone $$(PKG_URL_$$(call remove_special_chars,$$(notdir $(1)))) $$@
endef

# VIM plugins
VIM_OPT_PKGS = vim-markdown vim-markdown-folding vim-pandoc vim-pandoc-syntax \
               YouCompleteMe Smart-Tabs taboo.vim
VIM_START_PKGS = rotate.vim csv.vim php-foldexpr.vim SyntaxAttr vim-commentary \
                 vimteractive html5 php.vim vim_ingo_library SyntaxRange vim-cpp-modern \
                 vim-tmux keepcase.vim plantuml-syntax vim-fugitive \
                 lightline SimpylFold vim-bbye vim-perl Nvim-R \
                 vim-bookmarks vim-surround vim-javascript Vim-Jinja2-Syntax
TMUX_PKGS = tpm tmux-online-status tmux-cpu
TMUX_PKGS := $(addprefix tmux-plugins/,$(TMUX_PKGS))
VIM_PKGS = $(addprefix vim/pack/myplugins/opt/,$(VIM_OPT_PKGS)) \
           $(addprefix vim/pack/myplugins/start/,$(VIM_START_PKGS))
RANGER_PKGS = config_dir/ranger/plugins/ranger-archives
PKGS := $(VIM_PKGS) $(TMUX_PKGS) $(RANGER_PKGS)

# Package URLs
PKG_URL_vim_ingo_library=https://github.com/inkarkat/vim-ingo-library # Needed by SyntaxRange
PKG_URL_ranger_archives=https://github.com/maximtrp/ranger-archives
PKG_URL_vim_markdown:=https://github.com/plasticboy/vim-markdown
PKG_URL_vim_markdown_folding:=https://github.com/masukomi/vim-markdown-folding
PKG_URL_vim_pandoc:=https://github.com/vim-pandoc/vim-pandoc
PKG_URL_vim_pandoc_syntax:=https://github.com/vim-pandoc/vim-pandoc-syntax
PKG_URL_YouCompleteMe:=https://github.com/ycm-core/YouCompleteMe
PKG_URL_csv_vim:=https://github.com/chrisbra/csv.vim
PKG_URL_php_foldexpr_vim:=https://github.com/swekaj/php-foldexpr.vim
PKG_URL_SyntaxAttr:=https://github.com/vim-scripts/SyntaxAttr.vim
PKG_URL_vim_commentary:=https://github.com/tpope/vim-commentary
PKG_URL_vimteractive:=https://github.com/williamjameshandley/vimteractive
PKG_URL_html5:=https://github.com/othree/html5.vim
PKG_URL_php_vim:=https://github.com/StanAngeloff/php.vim
PKG_URL_SyntaxRange:=https://github.com/vim-scripts/SyntaxRange
PKG_URL_vim_cpp_modern:=https://github.com/bfrg/vim-cpp-modern
PKG_URL_vim_tmux:=https://github.com/tmux-plugins/vim-tmux
PKG_URL_keepcase_vim:=https://github.com/vim-scripts/keepcase.vim
PKG_URL_plantuml_syntax:=https://github.com/aklt/plantuml-syntax
PKG_URL_taboo_vim:=https://github.com/gcmt/taboo.vim
PKG_URL_vim_fugitive:=https://github.com/tpope/vim-fugitive
PKG_URL_lightline:=https://github.com/itchyny/lightline.vim
PKG_URL_SimpylFold:=https://github.com/tmhedberg/SimpylFold
PKG_URL_vim_bbye:=https://github.com/moll/vim-bbye
PKG_URL_vim_perl:=https://github.com/vim-perl/vim-perl
PKG_URL_Nvim_R:=https://github.com/jalvesaq/Nvim-R
PKG_URL_Smart_Tabs:=https://github.com/vim-scripts/Smart-Tabs
PKG_URL_vim_bookmarks:=https://github.com/MattesGroeger/vim-bookmarks
PKG_URL_vim_surround:=https://github.com/tpope/vim-surround
PKG_URL_vim_javascript:=https://github.com/pangloss/vim-javascript
PKG_URL_Vim_Jinja2_Syntax:=https://github.com/Glench/Vim-Jinja2-Syntax
PKG_URL_rotate_vim:=https://github.com/vim-scripts/rotate.vim
PKG_URL_tpm:=https://github.com/tmux-plugins/tpm
PKG_URL_tmux_online_status:=https://git::@github.com/tmux-plugins/tmux-online-status
PKG_URL_tmux_cpu:=https://github.com/tmux-plugins/tmux-cpu

PLATFORM=$(shell uname)
ifneq ($(PLATFORM),Linux)
	ROOT_CFG:=$(filter-out $(HOME)/.gitlinux $(HOME)/.XCompose,$(ROOT_CFG))
endif

all: deps

deps:
ifeq ($(MODE),perso)
	python3 -m pip install vobject # For view_vcal
	cpan -i XML::Feed IO::Socket::SSL Net::SSLeay HTTP::Tiny Getopt::Std Array::Utils
endif

check:
	shellcheck -s sh root_cfg/profile $(CFG_SHELL_FILES)
	shellcheck -s bash root_cfg/bash_profile
	shellcheck -s sh root_cfg/shellrc
	shellcheck -s bash root_cfg/bashrc

test:
	bash tests/test_100_source_profile.sh

test_docker:
	docker build -t cfg .
	docker run cfg

enter_docker:
	docker run -t -i --entrypoint='/bin/bash' cfg -l

# Define rules for cloning packages
$(foreach pkg,$(PKGS),$(eval $(call clone_git_repos,$(pkg))))

install: $(EMPTY_DIRS) $(CONFIG_DIR) $(PRIVATE_CONFIG_DIR) $(ROOT_CFG) $(PRIVATE_ROOT_CFG) $(HOME)/.ssh/config
	@# Clean broken links
	@for f in $$(find $(HOME) $(wildcard $(HOME)/.config) -maxdepth 1 -type l) ; do test -e $$f || rm "$$f" ; done

uninstall:
	$(RM) $(PRIVATE_ROOT_CFG) $(ROOT_CFG) $(CONFIG_DIR) $(PRIVATE_CONFIG_DIR)
	$(RM) -r vim/pack $(TMUX_PKG_DIR)

.PHONY: all test deps uninstall install check

$(EMPTY_DIRS):
	mkdir -p "$@"

ifneq ($(PRIVATE_DIR),)
$(HOME)/.%: $(PRIVATE_DIR)/root_cfg/%
	chmod -R og-rwx "$<"
	$(LN) "$<" "$@"
endif

$(HOME)/.lscolorsrc: make_lscolorsrc
	./$< >"$@"

$(HOME)/.ssh/config: ssh/config $(HOME)/.ssh
	$(LN) "$(CURDIR)/$<" "$@"

$(HOME)/.ssh:
	install -m u=rwx,go= -d "$@"

$(HOME)/.%: root_cfg/%
	$(LN) "$(CURDIR)/$<" "$@"

config_dir/ranger: $(RANGER_PKGS)

tmux-plugins: $(TMUX_PKGS)

$(HOME)/.tmux-plugins: tmux-plugins
	$(LN) "$(CURDIR)/$<" "$@"

vim: $(VIM_PKGS)

$(HOME)/.config/ranger: config_dir/ranger
	$(LN) "$(CURDIR)/$<" "$@"

$(HOME)/.vim: vim
	$(LN) "$(CURDIR)/$<" "$@"

ifneq ($(PRIVATE_DIR),)
$(HOME)/.config/%: $(PRIVATE_DIR)/config_dir/%
	chmod -R og-rwx "$<"
	@mkdir -p $$(dirname "$@")
	$(LN) "$<" "$@"
endif

$(HOME)/.config/%: config_dir/%
	@mkdir -p $$(dirname "$@")
	$(LN) "$(CURDIR)/$<" "$@"
