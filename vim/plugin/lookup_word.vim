" Look up a word into Wiktionary
"
" Usage
" <leader>de in visual mode for looking up selected word in English.
" <leader>de in normal mode to look up word under cursor in English.
" <leader>df in visual mode for looking up selected word in French.
" <leader>df in normal mode to look up word under cursor in French.
" <leader>di in visual mode for looking up selected word in Italian.
" <leader>di in normal mode to look up word under cursor in Italian.

" Set default values for global variables
if ! exists("g:lookup_word_chars")
	let g:lookup_word_chars = "A-Za-zçàéèêëîïöœûüß'-"
endif

function! s:DumpWiktionaryPage(word, lang)
	let l:link = 'https://'.a:lang.'.wiktionary.org/wiki/'.a:word
	call DumpWebpage(l:link)
	execute "normal! jV/".a:word."\<cr>kd"
endfunction

function! s:LookupWordOperator(type, lang)

	" Backup default register
	let saved_unnamed_register = @@

	" Select word to look for
	if a:type ==# 'v' || a:type ==# "\<c-v>" " visual mode
		execute "normal! `<v`>y"
	elseif a:type ==# 'char' " normal mode
		execute "normal! `[v`]y"
	else " Not handled
		return
	endif

	" Print Wiktionary page
	call s:DumpWiktionaryPage(@@, a:lang)

	" Restore default register
	let @@ = saved_unnamed_register

endfunction

function! s:LookupWordUnderCursor(lang)

	" Backup values
	let l:saved_unnamed_register = @@

	" Copy text under cursor
	let l:selected = SelectAndCopyAtCursorPos(g:lookup_word_chars)

	" View man page
	if l:selected
		call s:DumpWiktionaryPage(@@, a:lang)
	endif

	" Restore unnamed register
	let @@ = l:saved_unnamed_register

endfunction

" Operators for searching selected word in visual mode
vnoremap <leader>de :<c-u>call <SID>LookupWordOperator(visualmode(), 'en')<cr>
vnoremap <leader>df :<c-u>call <SID>LookupWordOperator(visualmode(), 'fr')<cr>
vnoremap <leader>di :<c-u>call <SID>LookupWordOperator(visualmode(), 'it')<cr>

" Operators for searching for word under cursor in normal node
nnoremap <leader>de :call <SID>LookupWordUnderCursor('en')<cr>
nnoremap <leader>df :call <SID>LookupWordUnderCursor('fr')<cr>
nnoremap <leader>di :call <SID>LookupWordUnderCursor('it')<cr>
