setlocal foldmethod=expr
setlocal foldexpr=GetShellFold(v:lnum)

function! GetShellFold(lnum)

	# Blank line
#	if getline(a:lnum) =~? '\v^\s*$'
#		return '-1'
#	endif

	" By default level is the same as previous line
	let level = '='

	" Get syntax highlighting groups
	let hiGroups = map(synstack(a:lnum, 1), 'synIDattr(v:val, "name")')
	if getline(a:lnum) =~ '^## ' " Start doxygen-style comment
		let level = '>1'
	elseif (len(hiGroups) > 0)

		" Look for function syntax group
		" 'shFunctionKey' only: 'function' keyword
		" Both 'shFunctionOne' and 'shFunction': old `foo()` notation.
		if index(hiGroups, 'shFunctionKey') > -1 || ( index(hiGroups, 'shFunctionOne') > -1 && index(hiGroups, 'shFunction') > -1)
			" Start fold level
			let level = '>1'
		endif
	endif

	return level
endfunction
