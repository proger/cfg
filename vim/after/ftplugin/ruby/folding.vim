setlocal foldmethod=expr
setlocal foldexpr=GetRubyFold(v:lnum)

function! GetRubyFold(lnum)

	" By default level is the same as previous line
	let level = '='

	" Get syntax highlighting groups
	let hiGroups = map(synstack(a:lnum, 1), 'synIDattr(v:val, "name")')
	if (len(hiGroups) > 0)

		" Look for function syntax group
		if index(hiGroups, 'rubyDefine') > -1
			" Start fold level
			let level = '>1'
		endif
	endif

	return level
endfunction
