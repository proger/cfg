syntax match SorceryVideoKey      /\C[blqckswgjy)]/
syntax match SorceryVideoDoor     /\C[}BLQCK0-9?\].]/
syntax match SorceryVideoEnemy    /\C[EGMHW]/
syntax match SorceryVideoLegend     / - .\+$/
syntax match SorceryVideoRegionName /==[A-Za-z ]\+==/
syntax match SorceryVideoCauldron !@!
syntax match SorceryVideoBadCauldron !#!
syntax match SorceryVideoStartingPoint !+!
syntax match SorceryVideoSorcerer !\$!
syntax match SorceryVideoRoof     !/|*\\!
syntax match SorceryVideoWater    /\~\+/
syntax match SorceryVideoFloorWall /[|_]/
syntax match SorceryVideoWeapon   /[!*%⁋&]/

highlight SorceryVideoBadCauldron ctermfg=89
highlight SorceryVideoCauldron ctermfg=27
highlight SorceryVideoDoor     ctermfg=130
highlight SorceryVideoKey      ctermfg=46
highlight SorceryVideoRoof     ctermfg=202
highlight SorceryVideoWater    ctermfg=75
highlight SorceryVideoFloorWall ctermfg=255
highlight SorceryVideoEnemy    ctermfg=196
highlight SorceryVideoWeapon   ctermfg=93
highlight SorceryVideoLegend   ctermfg=255
highlight SorceryVideoStartingPoint   ctermfg=226
highlight SorceryVideoSorcerer   ctermfg=11
highlight SorceryVideoRegionName ctermfg=207
