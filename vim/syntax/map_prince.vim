syntax match PrinceBreak     /_/
syntax match PrincePike      /╷/
syntax match PrinceFloorSwitch /▂/
syntax match PrinceGate      /┊/
syntax match PrinceMark      /[A-Z]/
syntax match PrinceText      / -\+ .*/
syntax match PrinceWall      /[█▁▔]/

highlight PrinceBreak     ctermfg=124
highlight PrinceFloorSwitch ctermfg=076
highlight PrinceGate      ctermfg=250
highlight PrincePike      ctermfg=160
highlight PrinceMark      ctermfg=207
highlight PrinceText      ctermfg=255
highlight PrinceWall      ctermfg=117
