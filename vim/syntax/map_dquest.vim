syntax match DQuestBridge    /=/
syntax match DQuestDesert    /\./
syntax match DQuestForest    /|/
syntax match DQuestHills     /\^/
syntax match DQuestMountains /#/
syntax match DQuestArmory    /!/
syntax match DQuestChest     /\$/
syntax match DQuestDoor      /@/
syntax match DQuestEntrance  /€/
syntax match DQuestRoof      /+/
syntax match DQuestInn       /&/
syntax match DQuestPlain     /_/
syntax match DQuestZebra     /\//
syntax match DQuestSwamp     /\*/
syntax match DQuestTown      /[A-Z?]/
syntax match DQuestText      / - .\+/
syntax match DQuestWater     /[~]/
syntax match DQuestCounter   /o/
syntax match DQuestWall      /░/

highlight DQuestArmory    ctermfg=196
highlight DQuestBridge    ctermfg=172
highlight DQuestChest     ctermfg=220
highlight DQuestCounter   ctermfg=208
highlight DQuestDesert    ctermfg=221
highlight DQuestDoor      ctermfg=94
highlight DQuestEntrance  ctermfg=129
highlight DQuestForest    ctermfg=28
highlight DQuestHills     ctermfg=94
highlight DQuestInn       ctermfg=160
highlight DQuestMountains ctermfg=244
highlight DQuestPlain     ctermfg=76
highlight DQuestRoof      ctermfg=251
highlight DQuestSwamp     ctermfg=30
highlight DQuestText      ctermfg=255
highlight DQuestTown      ctermfg=196
highlight DQuestWall      ctermfg=252
highlight DQuestWater     ctermfg=33
highlight DQuestZebra     ctermfg=21
