# vi: ft=bash cc=80

function test_profile {

    local -r shell="$1"
    local -r profile="$2"
    local -r log_tmp_file=$(mktemp -t tmp.XXXXXX)

    file="$TEST_DIR/../root_cfg/$profile"
    echo -e "Testing $profile with $shell..."
    (
        unset PROFILE_READ
        $shell -c ". \"$file\"" \
            || ( cat "$log_tmp_file" ; exit 100 )
        user_bin=$HOME/.local/bin
        [[ -d $user_bin ]] || tr ":" "\n" <<<"$PATH" | grep $user_bin || \
            { echo "Folder \"$user_bin\" is not in PATH." ; exit 101 ; }
     ) || {
         local -r status=$"?"
         echo "Test of $profile sourced by $shell returned $status." >&2
         exit "$status"
        }
     
     rm "$log_tmp_file"

     return 0
}

export TEST_CFG=1
export DEBUG=2
export TEST_DIR="$(realpath "$(dirname "$0")")"
export TIMESTAMP=1
 
test_profile sh profile
test_profile bash bash_profile
test_profile zsh zprofile

unset TEST_CFG
unset TEST_DIR
unset DEBUG
unset TIMESTAMP

exit 0
