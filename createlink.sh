# vi: ft=bash
FORCE=${FORCE:-}
SRC="$1"
DST="$2"

# On macos `-T` is not an option of `ln`, so we check first if destination
# already exists to avoid creating a symbolic link inside an existing directory
# of the same name.
if [[ -z $FORCE && -e $DST && ! -L $DST ]] ; then
    echo "Destination \"$DST\" exists and is not a symbolic link." >&2
    exit 1
fi

# Remove any existing link
[[ -L $DST ]] && unlink "$DST"

# Get relative path
SRC=$(realpath "$SRC")
if [[ $(uname) != Darwin ]] ; then
    SRC=$(realpath --relative-to=$(dirname "$DST") "$SRC")
fi

# Create link
declare -a ln_flags=(-sf)
[[ $(uname) == Linux ]] && ln_flags+=(-T)
#echo "ln ${ln_flags[@]} \"$SRC\" \"$DST\"" >&2
ln "${ln_flags[@]}" "$SRC" "$DST"
