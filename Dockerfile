FROM rockylinux:9

RUN yum install -y epel-release make git

ARG dir=/cfg

RUN rm $HOME/.bash_profile $HOME/.bashrc
RUN mkdir -p $HOME/.local/bin

COPY Makefile createlink.sh $dir/
COPY root_cfg/ $dir/root_cfg/
COPY config_dir/ $dir/config_dir/
COPY tests/ $dir/tests/
COPY ssh/ $dir/ssh/

WORKDIR $dir

RUN make install

ENTRYPOINT ["/cfg/tests/test_docker"]
